#bin
#test: ./run_a_day.sh 2017-12-03 EXP
# testing:
# 2017-02-24
# 2017-07-30
# 2017-08-30
#todo  country
#todo sourceName
#todo targetName

# ###### #
# Step 1 - Load the Canonical / EXP feeds dropped 
# ###### #

if [ -z "$1" ];then
   the_day=`date +%Y-%m-%d`
else
   the_day=$1
fi
echo $the_day

# QL2_DAY
q=`date -j -f "%Y-%m-%d %H" "$the_day 00" +%s`
ql2_day=$((($q-1009843200)/86400))
echo $ql2_day

if [ $2 = "EXP" ];then
    gzName=$the_day'_ExpediaListing.csv.gz'
    tmp=$the_day'_ExpediaListing.csv'
    csvName='FEED_EXP.csv'
    echo $gzName

    mkdir /Users/bertrand/snowflake/hotel_quality/data/$the_day

    aws s3 cp s3://expedia-hotel-inventory/$gzName /Users/bertrand/snowflake/hotel_quality/data/$the_day/. --profile dwadmin
    gunzip /Users/bertrand/snowflake/hotel_quality/data/$the_day/$gzName
    mv /Users/bertrand/snowflake/hotel_quality/data/$the_day/$tmp /Users/bertrand/snowflake/hotel_quality/data/$the_day/$csvName
    
    ls /Users/bertrand/snowflake/hotel_quality/data/$the_day/$csvName

elif [ $2 = "PRICE"];then
    gzName="Priceline_hotel-20171018.txt.gz"
    csvName="Priceline_hotel-20171018.txt"
    echo "not expedia"
fi


# ###### #
# Step 2 - Split the feed by country
# ###### #

R -e "rmarkdown::render('get_feeds_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='EXP'),output_file='output.html')"


# ###### #
# Step 3 - Select 'other' OTAs 'recent' activities by countries: <ota>_select_<country>.csv
# ###### #

R -e "rmarkdown::render('select_activity_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='BOOKING'),output_file='output.html')" 
R -e "rmarkdown::render('select_activity_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='AGODA'),output_file='output.html')" 
R -e "rmarkdown::render('select_activity_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='TRIVAGO'),output_file='output.html')" 
R -e "rmarkdown::render('select_activity_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='TAD'),output_file='output.html')" 
R -e "rmarkdown::render('select_activity_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='TRAVELREPUBLIC'),output_file='output.html')" 
R -e "rmarkdown::render('select_activity_by_country.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,site='HR2'),output_file='output.html')" 

# ###### #
# Step 4 - Matching
# ###### #
# CH
R -e "rmarkdown::render('match_hotels.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH',sourceType='FEED',sourceName='EXP',targetType='SF',targetName='BOOKING'),output_file='output.html')"
R -e "rmarkdown::render('match_hotels.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH',sourceType='FEED',sourceName='EXP',targetType='SF',targetName='HR2'),output_file='output.html')"
R -e "rmarkdown::render('match_hotels.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH',sourceType='FEED',sourceName='EXP',targetType='SF',targetName='AGODA'),output_file='output.html')"
R -e "rmarkdown::render('match_hotels.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH',sourceType='FEED',sourceName='EXP',targetType='SF',targetName='TRIVAGO'),output_file='output.html')"
R -e "rmarkdown::render('match_hotels.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH',sourceType='FEED',sourceName='EXP',targetType='SF',targetName='TAD'),output_file='output.html')"
R -e "rmarkdown::render('match_hotels.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH',sourceType='FEED',sourceName='EXP',targetType='SF',targetName='TRAVELREPUBLIC'),output_file='output.html')"

# ###### #
# Step 5 - List matching results
# ###### #

R -e "rmarkdown::render('list_matches.Rmd',params=list(the_day='$the_day',ql2_day=$ql2_day,country='CH'),output_file='output.html')"

