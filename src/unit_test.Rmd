---
title: "R Notebook"
output: html_notebook
---


```{r}
basepath<-"/Users/bertrand/snowflake/hotel_quality"
library(RNeo4j)
graph <- startGraph("http://localhost:7474/db/data/")
```

```{r unit_test}

check_properties_rel( 'EXP' , 'PRICE' , '24218' , '59115504' , 'HU_1' , 'HU' )

check_properties_rel( 'EXP' , 'PRICE' , '2963254' , '15699804' , 'HU_2' , 'HU' )

check_properties_rel( 'EXP' , 'PRICE' , '2689272' , '7091403' , 'HU_3' , 'HU' )

check_properties_rel( 'EXP' , 'PRICE' , '2689272' , '7091403' , 'HU_4' , 'HU' )

check_properties_rel( 'EXP' , 'PRICE' , '3547631' , '7094203' , 'HU_5' , 'HU' )

check_properties_rel( 'EXP' , 'PRICE' , '1423506' , '7088003' , 'HU_6' , 'HU' )

# t7 
check_properties_rel( 'EXP' , 'PRICE' , '858379' , '4900305' , 'HU_7' , 'HU' )

# t8 
check_properties_rel( 'EXP' , 'PRICE' , '2860854' , '7101603' , 'HU_8' , 'HU' )

# t9 - no match - R_hotelname empty then keep hotelname
check_not_linked( 'EXP' , 'PRICE' , '11645197' , 'HU_9' , 'HU' )

# t10 - no match
check_not_linked( 'EXP' , 'PRICE' , '4665575' , 'HU_10' , 'HU' )

# t11 
check_properties_rel( 'EXP' , 'PRICE' , '115330' , '7097903' , 'HU_11' , 'HU' )

# t12 
check_not_linked( 'EXP' , 'PRICE' , '3760259' , 'HU_12' , 'HU' )

# t13 - ZENIT HOTEL BALATON = OENIT HOTEL .... 
check_properties_rel( 'EXP' , 'PRICE' , '4255626' , '20210704' , 'HU_13' , 'HU' )

# t14 - HOTEL FABIUS PANZIO has not equivalence with PRICE
check_properties_rel( 'EXP' , 'PRICE' , '2506467' , '7095503' , 'HU_14' , 'HU' )

## SWISS - CH

check_properties_rel( 'EXP' , 'PRICE' , '2506467' , '7095503' , 'CH_1' , 'CH' )

```
```{r properties_id}
check_properties_rel<-function( source, target, p1 ,p2 , tNum , country ){
  
  query<- paste0( "MATCH (n:", source , " {COUNTRY:'",country,"',PROPERTY_ID:'", p1 , "'})-[r]->(m:",target," ) RETURN m.PROPERTY_ID,m.HOTELNAME,m.SOURCE;" )
  cc <- cypher(graph, query)

if("m.PROPERTY_ID" %in% colnames(cc)) {
  if ( cc$m.PROPERTY_ID == p2 ) { print ( c(tNum, " passed") )  } else { print ( c(tNum, " failed") ) }
  
} else { print ( c(tNum ," failed")) } }

```

```{r check_not_linked}
check_not_linked<-function( source, target, p1 , tNum, country ) {
  
query<- paste0("MATCH (n:",source," {COUNTRY:'", country,"',PROPERTY_ID:'", p1 , "'})-[r]-(m:",target,") RETURN m.PROPERTY_ID,r.confidence;" )

cc <- cypher(graph, query)

if("r.confidence" %in% colnames(cc)) {
  if ( cc$r.confidence < .90 ) { print ( c(tNum, " passed") )  } else { print ( c(tNum, " failed") ) }
  
} else { print ( c(tNum ," passed")) } }

```

```{r misc_neohelp}
cnt_nodes = "match (n) return count(n);"
cypher(graph, cnt_nodes)

cnt_edges = "match (n)-[r]->(m) return count(r);"
cypher(graph, cnt_edges)

cc = "Match (n:EXP{PROPERTY_ID:'11484251'})-[*1..4]->(m) WHERE (m.PROPERTY_ID<>n.PROPERTY_ID) return n.SOURCE,n.PROPERTY_ID,m.SOURCE,m.PROPERTY_ID;"

cc = "match (n:EXP{PROPERTY_ID:'15802627'})-[r:matches*1..3]-(m) return n.SOURCE,n.PROPERTY_ID,n.HOTELNAME,m.SOURCE,m.PROPERTY_ID,m.HOTELNAME;"
cypher(graph, cc)

#WHERE (game.state='INVITE' AND game.initiator <> 1) OR (game.state<>'INVITE') 


cypher(graph, "match (n)-[r]->(m) return count(r);" )
cypher(  graph , "MATCH (n:EXP{COUNTRY:'CH'})-[r]->(m) RETURN count(n);")

cypher(  graph , "MATCH (n {country:'HU'})-[r]->(m) DELETE r;" )

```