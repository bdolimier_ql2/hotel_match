# README #

### Hotel Matching ###

* Matching hotel between several sources
* beta V.1

### Steps ###

* Step 0 - OTA feeds dropped in day dir: <ota>_feed.csv
* Step 1 - Bring hotels past matches to sandbox: ACTIVE_<country>.csv / INACTIVE_<country>.csv / UNMATCHED_<country>.csv
* Step 2 - Split feeds by country: <ota>_feed_<country>.csv
* Step 3 - Select 'other' OTAs 'recent' activities by countries: <ota>_select_<country>.csv
* Step 4 - Isolates new Hotels: <ota>_new_<country>.csv
* Step 5 - Match new hotels (DEBUG_<country>.csv, MATCHING_<country>.csv, SCORING.csv)
* Step 6 - Update ACTIVE_<country>.csv / INACTIVE_<country>.csv

### TODO ###

* OTA's own replicate isolation
* Removed hotels with no acitivities since x days from ACTIVE list
* Consolidate country classification errors

